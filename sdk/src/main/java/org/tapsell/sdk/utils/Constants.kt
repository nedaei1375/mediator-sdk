package org.tapsell.sdk.utils

internal object Constants {

    const val PreferencesName = "tapsell.org"
    const val DBName = "tapsell.db"

    const val ENDPOINT_AD_NETWORKS = "api/v1/adnetworks"
    const val ENDPOINT_REWARDED_WATERFALL = "api/v1/rewarded"
}