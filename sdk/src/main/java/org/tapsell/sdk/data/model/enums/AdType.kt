package org.tapsell.sdk.data.model.enums

enum class AdType(val title: String) {
    UNITY("UnityAds"),
    TAPSELL("Tapsell")
}